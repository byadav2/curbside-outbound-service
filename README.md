# curbside-outbound-service


###CREATE:
````
curl --location --request POST 'http://localhost:8010/orders' \
--header 'Content-Type: application/json' \
--data-raw '{
    "storeId": "100",
    "storeName": "Sodimac Homecenter Independencia",
    "accountId": "byadav",
    "status": "Placed",
    "lineItems": [
        {
            "product": {
                "name": "iphone",
                "price": "999"
            },
            "price": "999"
        }
    ],
    "customer": {
        "firstname": "bipin",
        "lastname": "yadav"
    },
    "billingAddress": {
        "street": "The Colony",
        "city": "santiago",
        "country": "country"
    },
    "shippingAddress": {
        "street": "The Colony",
        "city": "santiago",
        "country": "country"
    }
}'
````

### Find by accountId
```
curl --location --request GET 'http://localhost:8010/orders/search/findByAccountId?accountId=byadav'
```

###GET
```
curl --location --request GET 'http://localhost:8010/orders/16'
```