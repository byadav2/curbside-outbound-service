package com.fb.curbside.domain;

import java.math.BigDecimal;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@EqualsAndHashCode(callSuper = true)
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Data
public class LineItem extends BaseEntity {

    @ManyToOne(cascade = CascadeType.ALL)
    private Product product;

    private BigDecimal price;
    private int amount;

}
