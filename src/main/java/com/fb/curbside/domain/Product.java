package com.fb.curbside.domain;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@EqualsAndHashCode(callSuper = true)
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Data
public class Product extends BaseEntity {

    private String name;
    private String description;
    private BigDecimal price;

    @ElementCollection
    private Map<String, String> attributes = new HashMap<>();
}
