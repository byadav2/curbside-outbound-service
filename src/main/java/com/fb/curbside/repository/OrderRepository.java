package com.fb.curbside.repository;

import java.util.List;

import com.fb.curbside.domain.Order;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "orders", path = "orders")
public interface OrderRepository extends CrudRepository<Order, Long> {

    List<Order> findByAccountId(@Param("accountId") String accountId);
}
